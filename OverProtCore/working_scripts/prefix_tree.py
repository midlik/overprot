from __future__ import annotations
import sys
from pprint import pprint
from pathlib import Path
import json
from collections import defaultdict
from dataclasses import dataclass, field
from typing import TypeVar, Generic, NamedTuple, TypeAlias, Any, Iterator, Iterable

from overprot.libs.lib_logging import Timing


T = TypeVar('T')

@dataclass
class Match(Generic[T]):
    key: str
    value: T

KeyStr: TypeAlias = str

# PrefixTreeNode: TypeAlias = tuple[Match[T]|None, dict[KeyStr, tuple]]  # The inner tuple is again PrefixTreeNone but I don't know how solve cyclic dependency

@dataclass
class PrefixTree(Generic[T]):
    match: Match[T]|None = field(default=None)
    root: dict[str, PrefixTree] = field(default_factory = dict)
    FILL_CHAR = '-'

    def add(self, key: KeyStr, value: T, offset: int = 0) -> None:
        if offset == len(key):
            self.match = Match(key, value)
        else:
            letter = key[offset]
            if letter not in self.root:
                self.root[letter] = PrefixTree()
            self.root[letter].add(key, value, offset=offset+1)
    @classmethod
    def from_items(cls, items: Iterable[tuple[KeyStr, T]]) -> PrefixTree[T]:
        result: PrefixTree[T] = PrefixTree()
        with Timing('Sort entries'):
            items = sorted(items)
        for key, value in items:
            result.add(key, value)
        return result
    def json(self):
        m = self.match.value if self.match != None else None  # type: ignore
        r = {letter: subtree.json() for letter, subtree in self.root.items()}
        return (m, r)
    def _gen_print_lines(self, depth: int, prefix: list[str], sep: str) -> Iterator[str]:
        if self.match is not None:
            yield f"{self.FILL_CHAR * (depth - len(prefix))}{''.join(prefix)}{sep}{self.match.value}"
            prefix = []
        for letter, sub in self.root.items():
            prefix.append(letter)
            yield from sub._gen_print_lines(depth + 1, prefix, sep)
            prefix = []
    def pprint(self, sep = ':', file=sys.stdout) -> None:
        for line in self._gen_print_lines(0, [], sep):
            print(line, file=file)
    # def _search(self, sample: str, offset: int) -> Iterator[str]:
    #     if offset == len(sample):
    #         if self.match is not None:
    #             yield
    #     letter = sample[offset]
    #     if letter in self.root:

    #     ...
    # def search(self, sample: str) -> Iterator[str]:
    #     ...

class WeirdSearcher(Generic[T]):
    sorted_keys: list[KeyStr]
    value_dict: dict[KeyStr, T]
    def __init__(self, items: Iterable[tuple[KeyStr, T]]) -> None:
        self.value_dict = dict(items)
        self.sorted_keys = sorted(self.value_dict.keys())
    def _binary_search(self, sample: KeyStr):
        '''Return index of the first key that is >= sample (or len(keys) if no such key)'''
        fro = 0
        to = len(self.sorted_keys)
        while to - fro >= 2:
            mid = (fro+to) // 2
            if self.sorted_keys[mid] < sample:
                fro = mid
            else:
                to = mid
        while fro < to and self.sorted_keys[fro] < sample:
            fro += 1
        if fro < len(self.sorted_keys):
            key = self.sorted_keys[fro]
            value = self.value_dict.get(key)
        else:
            key = None
            value = None
        return (fro, key, value)
    def search(self, prefix: KeyStr) -> tuple[int, Iterator[KeyStr, T]]:
        '''Return number of matches and iterator of matched key-value pairs'''
        last_letter = prefix[-1]
        sentinel = prefix[:-1] + chr(ord(last_letter)+1)  # word that would be just after, if not for suffixes (e.g. prefix=foo -> sentinel=fop)
        fro, *_ = self._binary_search(prefix)
        to, *_ = self._binary_search(sentinel)
        def generate_results():
            for i in range(fro, to):
                key = self.sorted_keys[i]
                value = self.value_dict[key]
                yield (key, value)
        return to-fro, generate_results()


def get_lines(filename: Path|str, lstrip = False, rstrip = False, rstrip_newline = False, nonempty = False) -> Iterator[str]:
    with open(filename) as f:
        for line in f:
            if lstrip:
                line = line.lstrip()
            if rstrip:
                line = line.rstrip()
            if rstrip_newline:
                line = line.rstrip('\n')
            if nonempty and line == '':
                continue
            yield line

def test_prefix_tree():
    with Timing('TOTAL'):
        with Timing('Preparing entries'):
            DIR = Path('/home/adam/Workspace/Python/OverProt/data/trie')
            items = []
            for line in get_lines(DIR/'cath_b.names.20201021', rstrip_newline=True, nonempty=True):
                id, name = line.split(' ', maxsplit=1)
                name = ' '.join(name.split()).upper()  # normalize whitespace and case
                while len(name)>=3:
                    items.append((name, id))
                    name = name[1:]
        with Timing('Creating Trie'):
            trie = PrefixTree.from_items(items)  # auto sorts
        with Timing('Saving as TXT'):
            with open(DIR/'cath_b_names.fulltrie.txt', 'w') as w:
                trie.pprint(sep='\t', file=w)
        with Timing('Saving as JSON - creating'):
            trie_json = trie.json()
        with Timing('Saving as JSON - dumping'):
            with open(DIR/'cath_b_names.fulltrie.json', 'w') as w:
                json.dump(trie_json, w, separators=(',',':'))
        with Timing('Loading JSON'):
            with open(DIR/'cath_b_names.fulltrie.json') as r:
                json.load(r)

def test_weird_searcher_1():
    # keys = list('abcdefghijpqrstuvwxy')
    keys = 'aha ahoj ahojki cau cauki cuzes dobry tepero zdar'.split()
    searcher = WeirdSearcher((k, k.upper()) for k in keys)

    queries = keys + '0 a ah ak b c ca ci cu echo wow  zzz'.split()
    for key in queries:
        n_matches, gen_matches = searcher.search(key)
        print(key, n_matches, *gen_matches)
def test_weird_searcher_2():
    with Timing('TOTAL'):
        with Timing('Preparing entries'):
            DIR = Path('/home/adam/Workspace/Python/OverProt/data/trie')
            items = []
            lines = list(get_lines(DIR/'cath_b.names.20201021', rstrip_newline=True, nonempty=True))
            for line in lines:
                id, name = line.split(' ', maxsplit=1)
                name = ' '.join(name.split()).upper()  # normalize whitespace and case
                while len(name)>=3:
                    items.append((name, id))
                    name = name[1:]
        print('len(lines):', len(lines))
        print('len(items):', len(items))
        with Timing('Creating WeirdSearcher'):
            searcher = WeirdSearcher(items)
        with Timing('Searching each'):
            for key, value in items:
                n, gen = searcher.search(key[:20])
                # print('----------', repr(key), n, *gen, sep='\n', end='\n\n')
            



if __name__ == '__main__':
    # test_prefix_tree()
    test_weird_searcher_1()
    test_weird_searcher_2()
